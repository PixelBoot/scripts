#!/bin/bash

mkdir -p ~/android/lineage-17.1
cd ~/android/lineage-17.1
repo init --depth=1 -u git://github.com/LineageOS/android.git -b lineage-17.1
repo sync -c -j4 --force-sync --no-clone-bundle --no-tags
git clone https://github.com/PixelBoot/android_device_huawei_angler -b lineage-17.1 device/huawei/angler
git clone https://github.com/PixelBoot/android_kernel_huawei_angler -b lineage-17.1 kernel/huawei/angler
git clone https://github.com/PixelBoot/lineage_vendor_huawei -b lineage-17.1 vendor/huawei
git clone https://github.com/LineageOS/android_hardware_sony_timekeep -b lineage-17.1 hardware/sony/tmekeep
cd ~/android/lineage-17.1/hardware/qcom/display
git remote add pixelboot https://github.com/PixelBoot/android_hardware_qcom_display
git fetch pixelboot
git cherry-pick 872a602e7b182811b5499f7ba0a7a34d436692c0
cd ~/android/lineage-17.1/hardware/qcom/gps
git remote add pixelboot https://github.com/PixelBoot/android_hardware_qcom_gps
git fetch pixelboot
git cherry-pick 9ba794ea9be28e3bb00901f6bdfda058305c6371
cd ~/android/lineage-17.1
source build/envsetup.sh
breakfast angler
brunch angler