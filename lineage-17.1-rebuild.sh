#!/bin/bash

cd ~/android/lineage-17.1
repo sync --force-sync
cd ~/android/lineage-17.1/hardware/qcom/display
git remote add pixelboot https://github.com/PixelBoot/android_hardware_qcom_display
git fetch pixelboot
git cherry-pick 872a602e7b182811b5499f7ba0a7a34d436692c0
cd ~/android/lineage-17.1/hardware/qcom/gps
git remote add pixelboot https://github.com/PixelBoot/android_hardware_qcom_gps
git fetch pixelboot
git cherry-pick 9ba794ea9be28e3bb00901f6bdfda058305c6371
cd ~/android/lineage-17.1
source build/envsetup.sh
breakfast angler
brunch angler